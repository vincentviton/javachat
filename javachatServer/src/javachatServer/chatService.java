package javachatServer;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;



@Path("/chatService")
public class chatService {

	// http://localhost:8080/javachatServer/rest/chatService/helloWord
	@Path("/helloWord")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response hello() {
		System.out.println("HelloWorld!!");
		return Response.status(200).entity("HelloWord").build();
		//return "HelloWord";
	}

	// http://localhost:8080/javachatServer/rest/chatService/getHistory
	@Path("/getHistory")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response getHistory() {

		Gson gson = new Gson();

		String historyJSON;
		try {
			
			historyJSON = gson.toJson(BDDUtils.getAllMessages());
			return Response.status(200).entity(historyJSON).build();
		} catch (Exception e) {
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.message = e.getMessage();
			e.printStackTrace();
			return Response.status(550).entity( gson.toJson(errorMessage)).build();
		}
			
			

		//return historyJSON;
	}

	// http://localhost:8080/javachatServer/rest/chatService/sendMessage
	@POST
	@Path("/sendMessage")
	@Consumes(MediaType.APPLICATION_JSON)
	// @Produces(MediaType.APPLICATION_JSON)
	public void sendMessage(String recu) {
		//System.out.println(recu);
		Gson gson = new Gson();
		Message message = gson.fromJson(recu, Message.class);

		BDDUtils.addMessage(message);

	}
	
	//
	@POST
	@Path("/checkForUniqueUserName")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkForUniqueUserName(String userName){
		
		
		Gson gson = new Gson();

		String resultString="";
		try {
			resultString = gson.toJson(BDDUtils.checkForUniqueUserName(gson.fromJson(userName, String.class)));
			return Response.status(200).entity(resultString).build();
		} catch (JsonSyntaxException e) {
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.message = e.getMessage();
			e.printStackTrace();
			return Response.status(550).entity( gson.toJson(errorMessage)).build();
		} catch (Exception e) {
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.message = e.getMessage();
			e.printStackTrace();
			return Response.status(550).entity( gson.toJson(errorMessage)).build();
		}
		
		
		
		
	}
	
	
	@POST
	@Path("/getConnectedUsers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConnectUsers(String userName){
		
		
		Gson gson = new Gson();

		String resultString = gson.toJson(BDDUtils.getAllActiveUsersNames(gson.fromJson(userName, String.class)));
		
	
		
		return Response.status(200).entity(resultString).build();
		
	}
	
	@POST
	@Path("/getNewMessagesSince")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNewMessagesSince(String time){
		
		
		
		
		Gson gson = new Gson();

		String resultString;
		try {
			resultString = gson.toJson(BDDUtils.getNewMessagesSince(gson.fromJson(time, Long.class)));
			
		
			
			return Response.status(200).entity(resultString).build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.message = e.getMessage();
			return Response.status(550).entity( gson.toJson(errorMessage)).build();
		}
		
		
	}

}
