package javachatServer;

import com.google.gson.Gson;

public class Main {

	public static void main(String[] args) {

		User user = new User();
		Message message = new Message();
		user.pseudo = "toto";
		message.user = user;
		message.time = 0;
		message.content = "bla";

		Gson gson = new Gson();

		System.out.println(gson.toJson(message));

	}

}
