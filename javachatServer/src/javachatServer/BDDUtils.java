package javachatServer;



import java.util.ArrayList;
import java.util.Date;


public class BDDUtils {

	//private static ArrayList<Message> myDB = new ArrayList<>();
	//private static  Hashtable<String, Long> connectedUsersNameTable = new Hashtable<>();

	public static void addMessage(Message message) {
		long temp = new Date().getTime();
		message.time = temp;
		
		//myDB.add(message);
		try {
			ConnexionJDBC.addMessage(message);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("FAILED TO ADD MESSAGE TO BDD");
			e.printStackTrace();
		}
		
	}

	public static ArrayList<Message> getAllMessages() throws Exception {
		return getNewMessagesSince(0);
	}
	
	@SuppressWarnings("finally")
	public static ArrayList<Message> getNewMessagesSince(long time) throws Exception {
		
		//try {
			
			return ConnexionJDBC.getAllMessagesSince(time);
//		}
//		catch (Exception e) {
//			System.out.println("FAILED TO GET NEW MESSAGES FROM BDD");
//			e.printStackTrace();
//		}
		
		
	/*	ArrayList<Message> recentMessages = new ArrayList<>();
		
		for (Message message : myDB ){
			if(message.time>time){
				recentMessages.add(message);
			}
		}
		
		return recentMessages;*/
	}
	
	
	
	public static ArrayList<String> getAllActiveUsersNames(String userName){
		
		/*connectedUsersNameTable.put(userName,  new Date().getTime());
		return getActiveUsers();*/
		
		try {
			ConnexionJDBC.updateUserTime(userName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		try {
			return ConnexionJDBC.getActiveUsers();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	private static ArrayList<String> getActiveUsers() throws Exception{
		
		/*ArrayList<String> activeUsersList = new ArrayList<>();
		
		 Set<String> keys = connectedUsersNameTable.keySet();
	        for(String key: keys){
	          
	        	if(new Date().getTime() - connectedUsersNameTable.get(key)<60000){
	        		activeUsersList.add(key);
	        	}
	        }
		
		return activeUsersList;*/
		return ConnexionJDBC.getActiveUsers();
		
	}
	
	public static Boolean checkForUniqueUserName(String userName) throws Exception{
	
		
		if(getActiveUsers().contains(userName)){
			return false;
		}
		
		else{
			
			//connectedUsersNameTable.put(userName,  new Date().getTime());
			
			User user = new User();
			user.pseudo = userName;
			user.lastActivityTime = new Date().getTime();
			
			try{
				ConnexionJDBC.addUser(user);
				}
			catch (Exception e){
				e.printStackTrace();
			}
			
			return true;
		}
		
		
		
	}

}
