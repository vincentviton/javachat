package javachatServer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;


public class ConnexionJDBC {
	
	//Version Wamp
	public static String URL= "jdbc:mysql://localhost:3308/javachat";
	public static final String LOGIN= "root";
	public static final String PASSWORD= "";
	
	

	
	
private final static String QUERY_ADD_USER = "INSERT INTO users(pseudo,lastActivityTime) VALUES (?, ?);";
	
	public static void addUser(User user) throws Exception {
		//System.out.println("In ConnexionBDD");
		Connection con= null;
		PreparedStatement stmt= null;
		try{
			
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD);
			
			//con= DriverManager.getConnection(URL);// La connexion
			stmt= con.prepareStatement(QUERY_ADD_USER);
			// Remplir la requ�te
			stmt.setString(1, user.pseudo);
			stmt.setLong(2, user.lastActivityTime);
			// Lancer la requ�te
			stmt.executeUpdate();
			
			//System.out.println(user.pseudo);
		} finally{
			// On ferme la connexion
			if(con!= null) {
				try{
					con.close();
				} 
				catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	

	
private final static String QUERY_UPDATE_USER_TIME = "UPDATE users SET lastActivityTime=? where id=?;";
	
	public static void updateUserTime(String username) throws Exception {
		//System.out.println("In ConnexionBDD USERTIME");
		Connection con= null;
		PreparedStatement stmt= null;
		try{
			
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD);
			
			//con= DriverManager.getConnection(URL);// La connexion
			stmt= con.prepareStatement(QUERY_UPDATE_USER_TIME);
			// Remplir la requ�te
			stmt.setLong(1, new Date().getTime());
			stmt.setInt(2, getUserIdFromName (username));
			// Lancer la requ�te
			stmt.executeUpdate();
			
		
		} finally{
			// On ferme la connexion
			if(con!= null) {
				try{
					con.close();
				} 
				catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private final static String QUERY_ADD_MESSAGE = "INSERT INTO messages(userID, content, time, iswizz) VALUES (?, ?, ?, ?);";
	
	public static void addMessage(Message message) throws Exception {
		//System.out.println("In ConnexionBDD Add Message");
		Connection con= null;
		PreparedStatement stmt= null;
		try{
			
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD);
			
			//con= DriverManager.getConnection(URL);// La connexion
			stmt= con.prepareStatement(QUERY_ADD_MESSAGE);
			// Remplir la requ�te
			stmt.setInt(1, getUserIdFromName(message.user.pseudo));
			stmt.setString(2, message.content);
			stmt.setLong(3, message.time);
			stmt.setBoolean(4, message.isWizz);
			// Lancer la requ�te
			stmt.executeUpdate();
			
			//System.out.println(message.content);
		} finally{
			// On ferme la connexion
			if(con!= null) {
				try{
					con.close();
				} 
				catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	private final static String QUERY_GET_USER_ID_FROM_NAME= "SELECT * FROM users WHERE pseudo='";
	public static int getUserIdFromName(String pseudo) throws Exception {
	Connection con= null;
	Statement stmt= null;
		try{
			//Pour travailler avec Tomcat et wamp Rajouter :
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
			stmt= con.createStatement();
			ResultSet rset= stmt.executeQuery(QUERY_GET_USER_ID_FROM_NAME+pseudo+"'");
			rset.next();
			return rset.getInt("id");
			
		}
		finally{
			if(con!= null) {// On ferme la connexion
				try{
					con.close();
				} catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private final static String QUERY_GET_USER_FROM_ID= "SELECT * FROM users WHERE id=";
	public static User getUserFromId(int id) throws Exception {
	Connection con= null;
	Statement stmt= null;
		try{
			//Pour travailler avec Tomcat et wamp Rajouter :
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
			stmt= con.createStatement();
			ResultSet rset= stmt.executeQuery(QUERY_GET_USER_FROM_ID+id);
			rset.next();
			return rsetToUser(rset);
			
		}
		finally{
			if(con!= null) {// On ferme la connexion
				try{
					con.close();
				} catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private final static String QUERY_GET_RECENT_MESSAGES= "SELECT * FROM messages where time>";
	public static ArrayList<Message> getAllMessagesSince(long time) throws Exception {
	Connection con= null;
	Statement stmt= null;
		try{
			//System.out.println("IN GET RECENT MESSAGES");
			//System.out.println(time);
			//Pour travailler avec Tomcatet wampRajouter :
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
			stmt= con.createStatement();
			ResultSet rset= stmt.executeQuery(QUERY_GET_RECENT_MESSAGES+time);
			ArrayList<Message> returnList = new ArrayList<>();
			while(rset.next()) {
			Message message = rsetToMessage(rset);
			returnList.add(message);
			}
	
			return returnList;
		}
		finally{
			if(con!= null) {// On ferme la connexion
				try{
					con.close();
				} catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	private final static String QUERY_GET_ACTIVE_USERS= "SELECT * FROM users where lastActivityTime>";
	public static ArrayList<String> getActiveUsers() throws Exception {
	Connection con= null;
	Statement stmt= null;
		try{
			//Pour travailler avec Tomcat et wamp Rajouter :
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			con= DriverManager.getConnection(URL, LOGIN, PASSWORD); //La connexion
			stmt= con.createStatement();
			ResultSet rset= stmt.executeQuery(QUERY_GET_ACTIVE_USERS+(new Date().getTime()-60000));
			ArrayList<String> returnList = new ArrayList<>();
			while(rset.next()) {
			User user = rsetToUser(rset);
			returnList.add(user.pseudo);
			}
			return returnList;
		}
		finally{
			if(con!= null) {// On ferme la connexion
				try{
					con.close();
				} catch(final SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	

	
	private static Message rsetToMessage(ResultSet rset) throws SQLException{
		
		final Message message = new Message();
		message.user = new User();
		
		try {
			
			message.user.pseudo = (getUserFromId(rset.getInt("userID")).pseudo);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		message.content = rset.getString("content");
		message.time = rset.getLong("time");
		message.isWizz = rset.getBoolean("iswizz");
		return message;
		}
	
	private static User rsetToUser(ResultSet rset) throws SQLException{
		final User user = new User();
		user.pseudo = (rset.getString("pseudo"));
		return user;
		}
	
	
}
