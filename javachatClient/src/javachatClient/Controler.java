package javachatClient;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Controler {

	private IHMChatBox chatBox;
	private IHMLoginChatBox loginChatBox;
	private User user;
	private User chatBot;

	final ArrayList<Message> history;
	ArrayList<String> userList;
	Timer timer;

	public Controler() {

		chatBot = new User();
		chatBot.pseudo = "ChatBot";
		userList = new ArrayList<>();
		history = new ArrayList<>();
		chatBox = new IHMChatBox(this);
		loginChatBox = new IHMLoginChatBox(this);
		reloadHistory();
		reloadUsersList();
		timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				reloadHistory();
				reloadUsersList();
				// chatBox.scrollToBottom();
			}

		}, 0, 2500);

	}

	public User getUser() {
		return user;
	}

	public void refreshScreen() {
		if (user == null) { // LOGIN IHM
			chatBox.setVisible(false);
			loginChatBox.setVisible(true);

		}

		else { // CHAT IHM

			chatBox.setVisible(true);
			loginChatBox.setVisible(false);
			chatBox.showHistory(history);
			chatBox.showConnectedUsers(userList);

		}
		chatBox.scrollToBottom();
	}

	public void onLogginBtPressed(String pseudo) {
		if (!"".equals(pseudo)) {
			try {
				pseudo = pseudoFormat(pseudo);
				if (WSUtils.checkForUniqueUserName(pseudo)) {
					user = new User();
					user.pseudo = pseudo;

				}
				else {
					loginChatBox.updateErrorLabel("Le pseudo est d�j� utilis�!");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else {
			loginChatBox.updateErrorLabel("Veuillez renseigner un pseudo!");
		}
		refreshScreen();

		/**
		 * if (!"".equals(pseudo)) { System.out.println("pseudo");
		 * System.out.println(pseudo); user = new User(); user.pseudo = pseudo;
		 * refreshScreen(); } else { loginChatBox.updateErrorLabel("Veuillez renseigner
		 * un pseudo valide!"); }
		 */

	}

	public void onClickBtSend(String messageContent) {
		if (!"".equals(messageContent)) {
			Message message = new Message();
			message.content = messageContent;
			message.user = user;
			message.isWizz = false;
			try {
				WSUtils.sendMessage(message);
				reloadHistory();
				chatBox.clearInputArea();
			} catch (Exception exception) {

				errorMessage("Impossible d'envoyer le message!");
				exception.printStackTrace();
			}

		}
		else {
			errorMessage("Veuillez remplir le champ Message!");
		}
		refreshScreen();
	}

	private void errorMessage(String errorText) {

		Message errorMessage = new Message();
		errorMessage.user = chatBot;
		errorMessage.content = errorText;
		errorMessage.time = 0;
		history.add(errorMessage);

	}

	public void reloadHistory() {
		try {
			long max = 0;
			boolean isAWizz = false;

			for (Message message : history) {
				if (message.time > max) {
					max = message.time;
				}
			}
			ArrayList<Message> lastTemp = WSUtils.getNewMessagesSince(max);
			if (user != null) {
				for (Message message : lastTemp) {

					if (message.isWizz && !(message.user.pseudo.equals(user.pseudo))) {
						isAWizz = true;
					}
				}
			}
			if (isAWizz == true) {
				chatBox.createWizz();
			}
			// history.addAll(lastTemp);
			if (lastTemp.size() > 0) {
				history.addAll(lastTemp);
				refreshScreen();
			}
		} catch (Exception e) {
			errorMessage("<color=red>Connexion au serveur impossible!</color>");
			e.printStackTrace();
			refreshScreen();
		}

	}

	public void reloadUsersList() {

		try {

			if (user != null) {

				ArrayList<String> temp = WSUtils.getConnectedUsers(user.pseudo);

				userList.clear();
				userList.addAll(temp);
				refreshScreen();
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			refreshScreen();
		}

	}

	public String pseudoFormat(String pseudo) {
		pseudo = pseudo.substring(0, 1).toUpperCase() + pseudo.substring(1).toLowerCase();
		return pseudo;
	}

	public void onClickBtWizz() {
		Message message = new Message();
		message.user = user;
		message.isWizz = true;
		message.content = "Wizz";

		try {
			WSUtils.sendMessage(message);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// A compl�ter (Wizz les autres utilisateurs, d�lai sur le wizz)
	/**
	 * public void createWizz() { System.out.println("Wizz3"); int
	 * pos_x=chatBox.getFrame().getBounds().getLocation().x; int
	 * pos_y=chatBox.getFrame().getBounds().getLocation().y;
	 *
	 * int [] po={0,2}; for (int i=0;i<7;i++) {
	 *
	 * chatBox.getFrame().setBounds(pos_x+po[i%2],pos_y+po[i%2],chatBox.getFrame().getBounds().getSize().width,chatBox.getFrame().getBounds().getSize().height);
	 * try { //donne l'effet de wizz Thread.sleep(35); System.out.println("Wizz3");
	 * } catch(Exception e12){ e12.printStackTrace(); } }
	 */
}
