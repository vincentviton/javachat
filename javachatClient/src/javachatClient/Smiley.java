package javachatClient;

import java.net.URL;

public class Smiley {

	public static String getSmiley(String message) {

		URL url = ClassLoader.getSystemResource("face.png");
		URL url1 = ClassLoader.getSystemResource("disappointed.png");
		URL url2 = ClassLoader.getSystemResource("smiling.png");
		URL url3 = ClassLoader.getSystemResource("persevering.png");

		String smiley = message;

		smiley = smiley.replace("(/)", "<img src=" + url1 + " width=16px height=16px>");

		smiley = smiley.replace("/)", "<img src=" + url3 + " width=16px height=16px>");

		smiley = smiley.replace("(::)", "<img src=" + url + " width=16px height=16px>");

		smiley = smiley.replace(":)", "<img src=" + url2 + " width=16px height=16px>");

		return smiley;

	}

}
