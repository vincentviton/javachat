package javachatClient;

import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

public class IHMChatBox {

	private JFrame frame;
	private JTextField textArea;
	private JButton btnSend;
	private JTextPane PanelUsers;
	private JTextPane areaHistory;
	private JButton btnWizz;
	private JScrollPane sbah;
	private JScrollPane sbpu;
	private static Controler controler;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					IHMChatBox window = new IHMChatBox(controler);
					window.frame.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IHMChatBox(Controler controler) {
		initialize();
		this.controler = controler;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		frame.setBounds(100, 100, 1430, 597);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		textArea = new JTextField();
		textArea.setBounds(326, 530, 617, 20);
		frame.getContentPane().add(textArea);

		btnSend = new JButton("Envoyer");
		btnSend.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				controler.onClickBtSend(textArea.getText());

			}
		});

		btnSend.setBounds(1001, 525, 132, 30);
		frame.getContentPane().add(btnSend);

		JLabel lblUsers = new JLabel("Utilisateurs");
		lblUsers.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsers.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsers.setBounds(10, 1, 170, 28);
		frame.getContentPane().add(lblUsers);

		JLabel lblMessage = new JLabel("Message :");
		lblMessage.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMessage.setHorizontalAlignment(SwingConstants.CENTER);
		lblMessage.setBounds(230, 528, 86, 20);
		frame.getContentPane().add(lblMessage);

		JLabel lblChat = new JLabel("Chat");
		lblChat.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblChat.setHorizontalAlignment(SwingConstants.CENTER);
		lblChat.setBounds(156, 0, 1258, 28);
		frame.getContentPane().add(lblChat);

		PanelUsers = new JTextPane();
		PanelUsers.setContentType("text/html");
		sbpu = new JScrollPane(PanelUsers);
		sbpu.setSize(190, 520);
		sbpu.setLocation(10, 30);
		PanelUsers.setEditable(false);
		PanelUsers.setBackground(Color.WHITE);
		PanelUsers.setBounds(10, 39, 170, 509);
		frame.getContentPane().add(sbpu);

		areaHistory = new JTextPane();
		areaHistory.setContentType("text/html");
		sbah = new JScrollPane(areaHistory);
		sbah.setLocation(210, 30);
		sbah.setSize(1194, 489);

		areaHistory.setEditable(false);
		areaHistory.setBackground(Color.GRAY);
		areaHistory.setBounds(208, 39, 1174, 474);
		frame.getContentPane().add(sbah);

		btnWizz = new JButton("Wizz");
		btnWizz.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				controler.onClickBtWizz();
			}
		});
		btnWizz.setBounds(1165, 525, 132, 30);
		frame.getContentPane().add(btnWizz);
	}

	public void clearInputArea() {
		textArea.setText("");
	}

	public void showHistory(ArrayList<Message> messageArrayList) {
		String historyString = "";
		Calendar calendar = Calendar.getInstance();

		for (Message message : messageArrayList) {
			if (message.isWizz == false) {
				calendar.setTimeInMillis(message.time);
				String TimeString = "";
				// Hour format 0X or XX
				int TimeStringHour = calendar.get(Calendar.HOUR_OF_DAY);
				if (TimeStringHour < 10) {
					TimeString = "0" + TimeStringHour;
				}
				else {
					TimeString = "" + TimeStringHour;
				}
				// Minutes format 0X or XX
				int TimeStringMinutes = calendar.get(Calendar.MINUTE);
				if (TimeStringMinutes < 10) {
					TimeString += ":0" + TimeStringMinutes + "";
				}
				else {
					TimeString += ":" + TimeStringMinutes;
				}
				// View messageTime
				historyString += "<font size=2>" + TimeString + "</font>" + "" + "<b> " + message.user.pseudo + "</b>"
						+ "" + "<i> dit</i> : " + Smiley.getSmiley(message.content) + "" + "<br>";
			}
		}
		areaHistory.setText(historyString);
	}

	public void showConnectedUsers(ArrayList<String> userList) {

		String connectedUsers = "";
		for (String user : userList) {
			connectedUsers += user + "<br>";
		}
		PanelUsers.setText(connectedUsers);
	}

	public void displayError(String errorMessage) {

		areaHistory.setText(areaHistory.getText() + "** " + errorMessage + "**\n");

	}

	public void setVisible(boolean check) {
		frame.setVisible(check);
	}

	public Object getBounds() {
		// TODO Auto-generated method stub
		return null;
	}

	public void createWizz() {
		int pos_x = frame.getBounds().getLocation().x;
		int pos_y = frame.getBounds().getLocation().y;

		int[] po = { 0, 2 };
		for (int i = 0; i < 7; i++) {

			frame.setBounds(pos_x + po[i % 2], pos_y + po[i % 2], frame.getBounds().getSize().width,
					frame.getBounds().getSize().height);
			try {
				// donne l'effet de wizz
				Thread.sleep(35);
			}
			catch (Exception e12) {
				e12.printStackTrace();
			}
		}
	}

	public void scrollToBottom() {
		JScrollBar vertical = sbah.getVerticalScrollBar();
		vertical.setValue(vertical.getMaximum());
	}

}
