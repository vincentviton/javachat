package javachatClient;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class WSUtils {

	final static String ROOT_PATH = "http://192.168.41.47:8080/javachatServer/rest/chatService/";

	public static String getHelloWorld() throws Exception {

		return OKhttpUtils.sendGetOkHttpRequest(ROOT_PATH + "helloWord");

	}

	public static void sendMessage(Message message) throws Exception {
		
			Gson gson = new Gson();

			String messageString = gson.toJson(message);

			OKhttpUtils.sendPostOkHttpRequest(ROOT_PATH + "sendMessage", messageString);
			

	}

	public static ArrayList<Message> getHistory() throws Exception {

		Gson gson = new Gson();

		String receivedListJSON = OKhttpUtils.sendGetOkHttpRequest(ROOT_PATH + "getHistory");

		ArrayList<Message> objectlist = gson.fromJson(receivedListJSON, new TypeToken<ArrayList<Message>>() {
		}.getType());

		return objectlist;

	}

	public static Boolean checkForUniqueUserName(String userName) throws Exception {
		Gson gson = new Gson();

		String resultString = OKhttpUtils.sendPostOkHttpRequest(ROOT_PATH + "checkForUniqueUserName",
				gson.toJson(userName));

		Boolean resultBool = gson.fromJson(resultString, Boolean.class);

		return resultBool;
	}

	public static ArrayList<String> getConnectedUsers(String userName) throws Exception {
		Gson gson = new Gson();

		String resultString = OKhttpUtils.sendPostOkHttpRequest(ROOT_PATH + "getConnectedUsers", gson.toJson(userName));

		ArrayList<String> resultList = gson.fromJson(resultString, new TypeToken<ArrayList<String>>() {
		}.getType());

		return resultList;
	}
	
	public static ArrayList<Message> getNewMessagesSince(long time) throws Exception{
		
		Gson gson = new Gson();

		String resultString = OKhttpUtils.sendPostOkHttpRequest(ROOT_PATH + "getNewMessagesSince", gson.toJson(time));
		
		ArrayList<Message> resultList = gson.fromJson(resultString, new TypeToken<ArrayList<Message>>() {
		}.getType());
		//System.out.println(resultString);
		return resultList;
	}
	

	/*
	 * public static Message makeStudent(String name) throws Exception{
	 *
	 * String studentString = OKhttpUtils.sendGetOkHttpRequest(ROOT_PATH +
	 * "helloWorldJsonWithParam?name='"+name+"'");
	 *
	 * Gson gson= new Gson(); Message student = gson.fromJson(studentString,
	 * Message.class);
	 *
	 * return student;
	 *
	 * }
	 *
	 * public static Message checkStudent(Message student) throws Exception{
	 *
	 * Gson gson = new Gson();
	 *
	 * String studentJSON = gson.toJson(student);
	 *
	 * String studentString = OKhttpUtils.sendPostOkHttpRequest(ROOT_PATH +
	 * "helloWorldAllJson",studentJSON);
	 *
	 * return gson.fromJson(studentString, Message.class);
	 *
	 * }
	 */

}
