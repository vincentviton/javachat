package javachatClient;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class IHMLoginChatBox {

	private JFrame frame;
	private JTextField textField;
	private JButton btnConnexion;
	private JLabel ErrorLabel;

	private static Controler controler;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					IHMLoginChatBox window = new IHMLoginChatBox(controler);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IHMLoginChatBox(Controler controler) {
		initialize();
		this.controler = controler;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 444, 352);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		textField = new JTextField();
		textField.setBounds(123, 137, 172, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblRentrezVotrePseudo = new JLabel("Rentrez votre pseudo SVP!");
		lblRentrezVotrePseudo.setHorizontalAlignment(SwingConstants.CENTER);
		lblRentrezVotrePseudo.setBounds(123, 106, 172, 20);
		frame.getContentPane().add(lblRentrezVotrePseudo);

		btnConnexion = new JButton("Connexion");
		btnConnexion.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				controler.onLogginBtPressed(textField.getText());
			}
		});
		btnConnexion.setBounds(151, 168, 117, 23);
		frame.getContentPane().add(btnConnexion);

		ErrorLabel = new JLabel("");
		ErrorLabel.setForeground(Color.RED);
		ErrorLabel.setHorizontalAlignment(SwingConstants.CENTER);
		ErrorLabel.setBounds(0, 202, 428, 20);
		frame.getContentPane().add(ErrorLabel);
	}

	public void setVisible(boolean check) {
		frame.setVisible(check);
	}

	public void updateErrorLabel(String errorMessage) {
		ErrorLabel.setText(errorMessage);
	}
}
