"use strict";
let user=[];
function IHMToggle(state, userLog){
    console.log(state);  
    var login = document.getElementById("login");
    var chatBox = document.getElementById("replace");
    var message = document.getElementById("send");
    if (state === "true"){
        user={
            pseudo: userLog
        };
        console.log("coucou");
        login.style.display="none";
        chatBox.style.display="flex";
        message.style.display="block";
    }
    else
    {
        login.style.display="";
        chatBox.style.display="none";
        message.style.display="none";
        alert("Pseudo déjà utilisé!");
    }
}

function IHMDisplayActiveUsers(users)
{
    let usersConnected = JSON.parse(users);
    let userName;
    let ul=document.createElement('ul');
    let userPanel=document.getElementById('listusers');
    userPanel.innerHTML="";
    for(userName of usersConnected)
    {
        let li=document.createElement('li');
        li.append(userName);
        li.style.listStyle="none";
        ul.append(li);
    }
    userPanel.append(ul);   
}